import { MessageModel } from '../../../message/message.model';

export type SearchMessagesResponse = {
  data: MessageModel[];
};
