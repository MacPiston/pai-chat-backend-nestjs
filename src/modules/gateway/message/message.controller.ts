import {
  Body,
  Controller,
  Get,
  InternalServerErrorException,
  Post,
  Session,
  UseGuards,
} from '@nestjs/common';
import { MessageService } from '../../message/message.service';
import { CreateMessageDto } from '../../message/create/create.dto';
import { ISession } from '../../../common/interface/session.interface';
import { CreateResult } from '../../message/create/create.result';
import { CreateMessageResponse } from './create/create.response';
import { SearchMessagesDto } from '../../message/search/search.dto';
import { SearchMessagesResult } from '../../message/search/search.result';
import { SearchMessagesResponse } from './search/search.response';
import { AuthGuard } from '../../../common/guards/auth.guard';

@Controller('messages')
@UseGuards(AuthGuard)
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @Post()
  async create(
    @Session() session: ISession,
    @Body() dto: CreateMessageDto,
  ): Promise<CreateMessageResponse> {
    const res = await this.messageService.create(session.user_id, dto);
    switch (res) {
      case CreateResult.Ok:
        return {
          created: true,
        };
      case CreateResult.Error:
        throw new InternalServerErrorException({ created: false });
    }
  }

  @Get()
  async search(
    @Session() session: ISession,
    @Body() dto: SearchMessagesDto,
  ): Promise<SearchMessagesResponse> {
    const res = await this.messageService.search(session.user_id, dto);
    switch (res.result) {
      case SearchMessagesResult.Ok:
        return {
          data: res.data,
        };
      case SearchMessagesResult.Error:
        throw new InternalServerErrorException({ created: false });
    }
  }
}
