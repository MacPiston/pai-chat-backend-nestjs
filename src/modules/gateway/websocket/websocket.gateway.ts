import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, WebSocket } from 'ws';
import { IncomingMessage } from 'http';

@WebSocketGateway()
export class WebsocketGateway
  implements OnGatewayConnection, OnGatewayDisconnect
{
  private connectedSockets = [];

  @WebSocketServer()
  private readonly server: Server;

  handleConnection(client: WebSocket, request: IncomingMessage) {
    console.log('connected', (request as any).session);
    client.send('Hello from NestJS');
  }

  handleDisconnect(client: WebSocket) {}
}
