import { Module } from '@nestjs/common';
import { UserController } from './user/user.controller';
import { MessageController } from './message/message.controller';
import { UserModule } from '../user/user.module';
import { MessageModule } from '../message/message.module';
import { WebsocketGateway } from './websocket/websocket.gateway';

@Module({
  imports: [UserModule, MessageModule],
  controllers: [UserController, MessageController],
  providers: [WebsocketGateway],
})
export class GatewayModule {}
