export type LoginResponse =
  | {
      loggedin: true;
      user_name: string;
      user_id: number;
    }
  | {
      loggedin: false;
      user_name?: never;
      user_id?: never;
    };
