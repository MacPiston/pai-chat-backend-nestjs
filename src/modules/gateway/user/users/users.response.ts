import { UserModel } from '../../../user/user.model';

export type UsersResponse =
  | {
      data: UserModel[];
      loggedin?: never;
    }
  | {
      data?: never;
      loggedin: false;
    };
