import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Post,
  Req,
  Session,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { UserService } from '../../user/user.service';
import { RegisterUserDto } from '../../user/register/register.dto';
import { RegisterResponse } from './register/register.response';
import { RegisterResult } from '../../user/register/register.result';
import { LoginResponse } from './login/login.response';
import { GetByNameResult } from '../../user/get-by-name/get-by-name.result';
import { ISession } from '../../../common/interface/session.interface';
import { LoginTestResponse } from './login-test/login-test.response';
import { LogoutResponse } from './logout/logout.response';
import { UsersResponse } from './users/users.response';
import { GetAllResult } from '../../user/get-all/get-all.result';
import { AuthGuard } from '../../../common/guards/auth.guard';

@Controller()
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  async register(
    @Body() userRegisterDto: RegisterUserDto,
  ): Promise<RegisterResponse> {
    const res = await this.userService.register(userRegisterDto);
    switch (res) {
      case RegisterResult.Ok:
        return {
          register: true,
        };
      case RegisterResult.UsernameTaken:
        throw new ConflictException({ register: false });
      case RegisterResult.Error:
        throw new InternalServerErrorException({ register: false });
    }
  }

  @Post('login/:name')
  async login(
    @Param('name') name: any,
    @Req() request: Request,
  ): Promise<LoginResponse> {
    if (typeof name !== 'string' || !name.length) {
      throw new BadRequestException({ loggedin: true });
    }

    const res = await this.userService.getByName(name);
    switch (res.res) {
      case GetByNameResult.Ok:
        request.session.loggedin = true;
        request.session.user_id = res.user.id;
        request.session.user_name = res.user.name;

        return {
          loggedin: true,
          user_id: res.user.id,
          user_name: res.user.name,
        };
      case GetByNameResult.NotFound:
        throw new NotFoundException({ loggedin: false });
      case GetByNameResult.Error:
        throw new InternalServerErrorException({ loggedin: false });
    }
  }

  @Get('login-test')
  @UseGuards(AuthGuard)
  async loginTest(): Promise<LoginTestResponse> {
    return {
      loggedin: true,
    };
  }

  @Get('logout')
  async logout(@Req() request: Request): Promise<LogoutResponse> {
    request.session.destroy((e) => {
      if (e) {
        throw new InternalServerErrorException({ loggedin: true });
      }
    });

    return {
      loggedin: false,
    };
  }

  @Get('users')
  @UseGuards(AuthGuard)
  async users(@Session() session: ISession): Promise<UsersResponse> {
    const res = await this.userService.getAll();
    switch (res.res) {
      case GetAllResult.Ok:
        return {
          data: res.data,
        };
      case GetAllResult.Error:
        throw new InternalServerErrorException({ data: [] });
    }
  }
}
