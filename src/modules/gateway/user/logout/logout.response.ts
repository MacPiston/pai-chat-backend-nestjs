export type LogoutResponse = {
  loggedin: false;
};
