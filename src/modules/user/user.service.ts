import { Inject, Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { RegisterUserDto } from './register/register.dto';
import { RegisterResult } from './register/register.result';
import { GetByNameResult } from './get-by-name/get-by-name.result';
import { GetAllResult } from './get-all/get-all.result';
import { UserModel } from './user.model';

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}

  async register(dto: RegisterUserDto): Promise<RegisterResult> {
    try {
      const user = await this.prisma.user.findUnique({
        where: {
          name: dto.name,
        },
      });
      if (user) {
        return RegisterResult.UsernameTaken;
      }

      await this.prisma.user.create({
        data: {
          name: dto.name,
        },
      });
      return RegisterResult.Ok;
    } catch (e) {
      console.error(e);
      return RegisterResult.Error;
    }
  }

  async getByName(
    name: string,
  ): Promise<{ res: GetByNameResult; user?: UserModel }> {
    try {
      const user = await this.prisma.user.findUnique({
        where: {
          name,
        },
      });
      if (!user) {
        return {
          res: GetByNameResult.NotFound,
        };
      }

      return {
        res: GetByNameResult.Ok,
        user: new UserModel(user),
      };
    } catch (e) {
      console.error(e);
      return {
        res: GetByNameResult.Error,
      };
    }
  }

  async getAll(): Promise<{ res: GetAllResult; data?: UserModel[] }> {
    try {
      const users = await this.prisma.user.findMany();

      return {
        res: GetAllResult.Ok,
        data: users.map((user) => new UserModel(user)),
      };
    } catch (e) {
      console.error(e);
      return {
        res: GetAllResult.Error,
      };
    }
  }
}
