export enum GetByNameResult {
  Ok,
  NotFound,
  Error,
}
