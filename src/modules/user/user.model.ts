import { User } from '@prisma/client';

export class UserModel {
  id: number;
  name: string;

  constructor(entity: User) {
    this.id = entity.id;
    this.name = entity.name;
  }
}
