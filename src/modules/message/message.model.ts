import { Message } from '@prisma/client';

export class MessageModel {
  id: number;
  authorId: number;
  receiverId: number;
  content: string;
  createdAt: Date;

  constructor(entity: Message) {
    this.id = entity.id;
    this.authorId = entity.authorId;
    this.receiverId = entity.receiverId;
    this.content = entity.content;
    this.createdAt = entity.createdAt;
  }
}
