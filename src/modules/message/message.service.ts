import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './create/create.dto';
import { CreateResult } from './create/create.result';
import { PrismaService } from '../prisma/prisma.service';
import { SearchMessagesDto } from './search/search.dto';
import { SearchMessagesResult } from './search/search.result';
import { Message, Prisma, User } from '@prisma/client';
import { MessageModel } from './message.model';

@Injectable()
export class MessageService {
  constructor(private readonly prisma: PrismaService) {}

  async create(userId: number, dto: CreateMessageDto): Promise<CreateResult> {
    try {
      await this.prisma.message.create({
        data: {
          author: {
            connect: {
              id: userId,
            },
          },
          receiver: {
            connect: {
              id: dto.message_to_user_id,
            },
          },
          content: dto.message_text,
        },
      });
      return CreateResult.Ok;
    } catch (e) {
      console.error(e);
      return CreateResult.Error;
    }
  }

  async search(
    userId: number,
    dto: SearchMessagesDto,
  ): Promise<{ result: SearchMessagesResult; data?: MessageModel[] }> {
    try {
      const where: Prisma.MessageWhereInput = {};
      if (dto.message_to_user_id) {
        where.receiverId = dto.message_to_user_id;
      }

      const messages = await this.prisma.message.findMany({
        where,
        orderBy: {
          createdAt: 'desc',
        },
      });

      return {
        result: SearchMessagesResult.Ok,
        data: messages.map((message) => new MessageModel(message)),
      };
    } catch (e) {
      console.error(e);
      return { result: SearchMessagesResult.Error };
    }
  }
}
