export enum CreateResult {
  Ok = 'Ok',
  UserNotFound = 'UserNotFound',
  Error = 'Error',
}
