import { IsNotEmpty } from 'class-validator';

export class CreateMessageDto {
  @IsNotEmpty()
  message_text: string;

  @IsNotEmpty()
  message_to_user_id: number;
}
