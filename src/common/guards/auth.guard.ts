import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest() as Request;

    if (!('loggedin' in request.session) || !request.session.loggedin) {
      throw new UnauthorizedException({ loggedin: false });
    }

    return true;
  }
}
