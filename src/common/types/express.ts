import { ISession } from '../interface/session.interface';

declare module 'express-session' {
  interface SessionData extends Partial<ISession> {}
}
