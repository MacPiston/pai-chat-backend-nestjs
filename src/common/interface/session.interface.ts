export interface ISession {
  loggedin: boolean;
  user_id: number;
  user_name: string;
}
